package state;

import machine.Coins;
import machine.VendingMachine;

import java.util.List;

public abstract class State {
    VendingMachine vendingMachine;

    public State(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    public abstract String handleTransaction(String name, List<Coins> coinsList);
}
