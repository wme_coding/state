package state;

import machine.Coins;
import machine.VendingMachine;
import machine.VendingService;

import java.util.List;

public class NoChargeState extends State{
    public NoChargeState(VendingMachine vendingMachine) {
        super(vendingMachine);
    }

    @Override
    public String handleTransaction(String name, List<Coins> coinsList) {
        try {
            vendingMachine.makeChange(VendingService.getSumOfAllMoneyTaken(coinsList));
            //returning cash
            vendingMachine.removeProduct(name);
            return name;
        } catch (Exception e) {
            return "Returning money";
        }
    }
}
