package machine;

public enum Coins {
    ONE(1), TWO(2), FIVE(5), TEN(10);

    private int amount;

    Coins(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}
