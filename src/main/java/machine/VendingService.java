package machine;

import java.util.List;

public class VendingService {
    public static int getSumOfAllMoneyTaken(List<Coins> coinsList){
        int sum = 0;
        for(Coins c: coinsList){
            sum += c.getAmount();
        }
        return sum;
    }
}
