package machine;

import state.State;

import java.util.*;

public class VendingMachine {
    private State state;
    private Map<Coins, Integer> coinsInside;
    private Map<String, Integer> productPrice;

    public VendingMachine(State state) {
        this.state = state;
        productPrice = new HashMap<>();
        coinsInside = new HashMap<>();
        coinsInside.put(Coins.ONE, 0);
        coinsInside.put(Coins.TWO, 0);
        coinsInside.put(Coins.FIVE, 0);
        coinsInside.put(Coins.TEN, 0);
    }

    public void addProduct(String name, int price){
        productPrice.put(name, price);
    }

    public List<Coins> makeChange(int change) {
        int remainingChange = change;

        int oneCount = coinsInside.get(Coins.ONE);
        int twoCount = coinsInside.get(Coins.TWO);
        int fiveCount = coinsInside.get(Coins.FIVE);
        int tenCount = coinsInside.get(Coins.TEN);

        Coins[] sortedCoinsByCount = new Coins[oneCount + twoCount + fiveCount + tenCount];
        Arrays.fill(sortedCoinsByCount,0,tenCount,Coins.TEN);
        Arrays.fill(sortedCoinsByCount,tenCount,tenCount + fiveCount,Coins.FIVE);
        Arrays.fill(sortedCoinsByCount,tenCount + fiveCount,sortedCoinsByCount.length - oneCount - 1,Coins.TWO);
        Arrays.fill(sortedCoinsByCount,tenCount + fiveCount + twoCount,sortedCoinsByCount.length - 1,Coins.ONE);
        List<Coins> coins = Arrays.asList(sortedCoinsByCount);
        List<Coins> res = new LinkedList<>();
        for(int i = 0; i < coins.size() && remainingChange > 0; ++i){
            Coins coin = coins.get(i);
            if(coin.getAmount() >= remainingChange){
                res.add(coin);
                remainingChange -= coin.getAmount();
            }
        }

        if(remainingChange > 0){
            throw new IllegalArgumentException("not enough cash for change");
        }
        return res;
    }

    public void removeProduct(String name){
        productPrice.remove(name);
    }

    //Here we can buy a product
    public void buyProduct(String name, List<Coins> coins){
        state.handleTransaction(name, coins);
    }


}

